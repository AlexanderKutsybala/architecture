package com.seriabov.fintecharch.model.repo;

import android.arch.lifecycle.LiveData;

import com.seriabov.fintecharch.model.data.ApiResponse;

public interface Repository {
    LiveData<ApiResponse> getCoinsInfo();
}
