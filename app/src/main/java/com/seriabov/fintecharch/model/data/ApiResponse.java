package com.seriabov.fintecharch.model.data;

import java.util.List;

public class ApiResponse {
    private List<CoinInfo> mCoinInfos;
    private Throwable error;

    public ApiResponse(List<CoinInfo> coinInfos) {
        this.mCoinInfos = coinInfos;
        this.error = null;
    }
    public ApiResponse(Throwable error) {
        this.error = error;
        this.mCoinInfos = null;
    }

    public List<CoinInfo> getCoinInfos() {
        return mCoinInfos;
    }

    public Throwable getError() {
        return error;
    }
}
