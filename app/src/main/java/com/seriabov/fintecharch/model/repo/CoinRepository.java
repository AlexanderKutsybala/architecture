package com.seriabov.fintecharch.model.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.seriabov.fintecharch.BuildConfig;
import com.seriabov.fintecharch.model.data.CoinInfo;
import com.seriabov.fintecharch.model.data.ApiResponse;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoinRepository implements Repository {

    private Api apiService;
    private MutableLiveData<ApiResponse> coinInfo;

    @Override
    public LiveData<ApiResponse> getCoinsInfo() {
        coinInfo=new MutableLiveData<>();
        loadCoinInfo();
        return coinInfo;
    }

    private void loadCoinInfo() {

        initRetrofit();
        apiService.getCoinsList()
                .enqueue(new Callback<List<CoinInfo>>() {
                    @Override
                    public void onResponse(Call<List<CoinInfo>> call, Response<List<CoinInfo>> response) {

                        coinInfo.setValue(new ApiResponse(response.body()));
                        Log.i("YU","OKKKK");
                    }

                    @Override
                    public void onFailure(Call<List<CoinInfo>> call, Throwable t) {

                        coinInfo.setValue(new ApiResponse(t));
                        Log.i("YU","Err");
                    }
                });

    }

    private void initRetrofit() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        apiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }
}
