package com.seriabov.fintecharch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.seriabov.fintecharch.model.data.ApiResponse;
import com.seriabov.fintecharch.model.repo.CoinRepository;
import com.seriabov.fintecharch.model.repo.Repository;


public class MyViewModel extends ViewModel {

    private LiveData<ApiResponse> coinInfo;
    private Repository mRepository;
    private boolean flag;

    public MyViewModel() {
        flag = false;
        mRepository = new CoinRepository();
    }

    public void setFlag(boolean flag){
        this.flag=flag;
    }

    public LiveData<ApiResponse> getCoinInfo() {

        if (coinInfo == null || coinInfo.getValue().getError()!=null || flag==true) {

            coinInfo=mRepository.getCoinsInfo();
        }
        return coinInfo;
    }

}
